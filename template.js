'use strict';

var fs = require('fs'),
    config = require('./config');

function makeTestTemplate(jsons) {
    let result = {};
    jsons.forEach(json=> {
        for (let property in json) {
            if (result[property]==undefined) {
                result[property] = json[property]
            }
            else {
                if (result[property] !== json[property] && property != 'TemplateName' && !Array.isArray(json[property])) {
                    console.log(`Несовпадение значений в поле ${property} \nСмерженое: ${JSON.stringify(result[property])}\nНовое: ${JSON.stringify(json[property])}`);
                }
                if (Array.isArray(json[property])) {
                    var arr =  result[property].concat(json[property]);
                    result[property] = [makeTestTemplate(arr)];
                }
            }
        }
    });


    return result;
}

module.exports = {
    makeTestTemplate
};

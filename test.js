'use strict';

var exec = require('child_process').execFile,
    uid = require('uid'),
    fs = require('fs'),
    path = require('path'),
    eachSeries = require('async/eachSeries'),
    template = require('./template'),
    config = require('./config');
/*
 Сгенерить с помощью утилиты шаблоны данных
 Integrum.Rabota.MailTest.exe -gen
 Отправить шаблоны на почту
 Integrum.Rabota.MailTest.exe -f test.json -t Integrum.Rabota.Business.Wrappers.MailModels.MainPageRecallContext,Integrum.Rabota.Business
 */

function getTestFileNames(folder) {
    if (!folder) console.log('Ошибка. В getTestFileNames нужно указать папку, из которой брать файлы');
    return new Promise((resolve, reject)=> {
        fs.readdir(folder, (err, items)=> {
            var files = items
                .filter(item=>!fs.lstatSync(path.join(folder, item)).isDirectory());
            resolve(files);
        });
    })
}

function execMailTestUtil(files) {
    return new Promise((resolve, reject)=> {
        eachSeries(files, (file, callback)=> {
                exec(config.mailTestUtilExe, ['-f', path.join(config.tests, file), '-t', file.replace('_', ',').replace('.json', '')], (err, stdout, stderr)=> {
                    if (stderr) console.log(`${file}\n${stderr}`);
                    callback(err, stdout);
                })
            }, (err, result)=> {
                if (err) reject(err);
                else resolve(result);
            }
        );
    })

}

function readFiles(fileNames) {
    var results = [],
        promises = [];
    return new Promise((resolve, reject)=> {
        fileNames.forEach(fileName=> {
            var promise = new Promise(function (resolve, reject) {
                fs.readFile(path.join(config.emptyTemplates, fileName), 'utf8', function (err, data) {
                    if (err) reject(err);
                    results.push({fileName: fileName, data: JSON.parse(data)});
                    resolve();
                });
            });

            promises.push(promise);
        });
        Promise
            .all(promises)
            .then(()=> {
                resolve(results)
            })
            .catch(err=>reject(err))
    })
}

function createTestData(jsons) {
    return new Promise((resolve, reject)=> {
        fs.readFile(config.filledTemplate, 'utf8', function (err, data) {
            if (err) throw err;
            var testData = JSON.parse(data);
            jsons.forEach(json=> {
                json.data = fillTemplate(json.data, testData);
                fs.writeFileSync(path.join(config.tests, json.fileName), JSON.stringify(json.data, null, 2))
            });
            resolve();
        });
    })

}

function fillTemplate(template, testData) {
    for (let property in template) {
        if (property != 'TemplateName') {
            if (property == 'Header') {
                template[property] = testData['TemplateName'];
            }
            else if (property == 'ToEmail') {
                template[property] = config.email;
            }
            else if (!Array.isArray(template[property])) {
                template[property] = testData[property]
            }
            else {
                var fieldSample = template[property][0];
                testData[property].forEach(testData=> {
                    var fieldFilled = fillTemplate(fieldSample, testData);
                    template[property].push(fieldFilled)
                })
            }
        }
    }
    return template;
}

function createTests() {
    getTestFileNames(config.emptyTemplates).then(files=> {
        readFiles(files).then(jsons=> {
            createTestData(jsons)
                .then(()=> {
                    console.log(`В папке "${config.tests}" созданы шаблоны, заполненные тестовыми данными`);
                    process.exit(0)
                })
                .catch(err=> {
                    console.log(`Ошибка при создании в папке "${config.tests}" шаблонов, заполненных тестовыми данными`);
                    process.exit(1);
                });
        })
    });
}

function generateEmptyTemplates() {
    exec(config.mailTestUtilExe, ['-gen'], {cwd: config.emptyTemplates}, (err, stdout, stderr) => {
        if (err) {
            console.log(`Во время генерации шаблонов данных для писем произошла ошибка \n${err}\nНа всякий случай можно проверить папку с результатами"${config.emptyTemplates}"`);
            process.exit(1);
        } else {
            console.log(`Пустые шаблоны данных для писем сохранены в папке "${config.emptyTemplates}"`);
            process.exit(0);
        }
    })
}

function mergeEmptyTemplates() {
    getTestFileNames(config.emptyTemplates).then(files=> {
        readFiles(files).then(jsons=> {
                var result = template.makeTestTemplate(jsons.map(json=>json.data)),
                    extension = path.extname(config.filledTemplate),
                    folder = path.dirname(config.filledTemplate),
                    fileName = path.basename(config.filledTemplate, extension),
                    filledTemplateFileName = `${fileName}-new-${uid()}.json`,
                    resultFilePath = path.join(folder, filledTemplateFileName);

                fs.writeFileSync(resultFilePath, JSON.stringify(result, null, 2));
                console.log(`Создан объединенный тестовый шаблон "${resultFilePath}"`);
                process.exit(0);
            })
            .catch(err=> {
                console.log(err);
                process.exit(1);
            });
    });
}

function runTests() {
    getTestFileNames(config.tests).then(files=> {

        execMailTestUtil(files).then(()=> {
            console.log(`Письма отправленны на почту "${config.email}"`);
            process.exit(0);
        }).catch(err=> {
            console.log(`Во время отправки писем на почту "${config.email}" произошла ошибка \n${err}`);
            process.exit(1);
        })
    })
}


//generateEmptyTemplates();
//mergeEmptyTemplates();
//createTests();
runTests();

module.exports = {
    generateEmptyTemplates,
    mergeEmptyTemplates,
    createTests,
    runTests
};